output "db_address" {
    value       = aws_db_instance.database.address
    description = "Database address"
}

output "port" {
    value       = aws_db_instance.database.port
    description = "Database port"
}