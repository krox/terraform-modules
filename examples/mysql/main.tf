terraform {
  # Require any 0.12.x version of Terraform
  required_version = ">= 0.12, < 0.13"
}


provider "aws" {
  region    = "eu-west-2"
  
  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

module "mysql" {
  source = "../../data-stores/mysql"
  
  db_name       = var.db_name
  db_password   = var.db_password
  
  
}
