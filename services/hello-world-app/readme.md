# Hello World App 

This folder contains an example [Terraform](https://www.terraform.io/) configuration that defines a module for 
deploying a simple "Hello, World app" across a cluster of web servers (using [EC2](https://aws.amazon.com/ec2/) 
and [Auto Scaling](https://aws.amazon.com/autoscaling/)) with a load balancer (using 
[ELB](https://aws.amazon.com/elasticloadbalancing/)) in an [Amazon Web Services (AWS) 
account](http://aws.amazon.com/).