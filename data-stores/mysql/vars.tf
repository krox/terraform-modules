variable "db_password" {
  type          = string
  description   = "The password for the database"
}

variable "db_name" {
  description   = "Name of the database"
  type          = string

}

variable "db_allocated_storage" {
  description = "Allocated storage in GB"
  default     = 10
  type        = number
}

variable "instance_class" {
  description = "Instance class to be used for the db"
  default     = "db.t2.micro"
  type        = string
}

variable "username" {
  description = "DB username"
  default     = "admin"
  type        = string
}

