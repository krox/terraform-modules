variable "server_port" {
  description = "The port the server uses for HTTP requests"
  type        = number
  default     = 8080
}

variable "cluster_name" {
  description = "Name of the webserver cluster"
  type        = string
}

variable "min_size" {
  description = "Min number of AMI instances in cluster"
  default = 2
}

variable "max_size" {
  description = "Max number of AMI instances in cluster"
  default = 4
}

variable "instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)"
  type        = string
}

variable "custom_tags" {
  description   = "Custom tags to set on the instance in the ASG"
  default       = {}
  type          = map(string)
}

variable "ami" {
  description = "The AMI to run in the cluster"
  default     = "ami-04de2b60dd25fbb2e"
  type        = string
}


variable "subnet_ids" {
  description = "The subnet IDs to deploy to"
  type        = list(string)
}

variable "target_group_arns" {
  description   = "The ARNs of ELB target groups in which to register Instances"
  default       = []
  type          = list(string)
}

variable "health_check_type" {
  description   = "The type of health check to perform. Must be one of: EC2, ELB."
  default       = "ELB"
  type          = string
}

variable "user_data" {
  description   = "The user data script to run in each instance"
  default       = null
  type          = string
}



