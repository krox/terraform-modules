variable "alb_name" {
  description = "The name to use for ALB"
  type        = string
}

variable "subnet_ids" {
  description   = "Subnet IDs where to deploy to"
  type          = list(string)
}
