variable "cluster_name" {
  description   = "The name of the ASG and all its resources"
  default       = "example_asg"
  type          = string
}
