output "db_address" {
    value       = module.mysql.db_address
    description = "Database address"
}

output "port" {
    value       = module.mysql.port
    description = "Database port"
}