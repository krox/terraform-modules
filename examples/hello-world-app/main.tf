terraform {
  # Require any 0.12.x version of Terraform
  required_version = ">= 0.12, < 0.13"
}


provider "aws" {
  region    = "eu-west-2"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

module "hello-world-app" {
  source = "../../services/hello-world-app"

  environment               = "example"
  db_remote_state_bucket    = "damo-terraform-up-and-running-state"
  db_remote_state_key       = "prod/data-stores/mysql/terraform.tfstate"
  instance_type             = "t2.micro"
  min_size                  = 1
  max_size                  = 1
  server_text               = "Hellow World example!"


  
}
