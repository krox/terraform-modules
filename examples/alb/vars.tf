variable "alb_name" {
  description = "The name to use for ALB"
  default     = "example-alb"
  type        = string
}
