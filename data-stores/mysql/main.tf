terraform {
  # Require any 0.12.x version of Terraform
  required_version = ">= 0.12, < 0.13"
}

resource "aws_db_instance" "database" { 
  allocated_storage = var.db_allocated_storage
  engine            = "mysql"
  identifier_prefix = "tf-db"
  instance_class    = var.instance_class
  skip_final_snapshot = true # to be able to delete db without creating a snaphsot


  name              = "${var.db_name}_database"
  username          = var.username
  password          = var.db_password
}