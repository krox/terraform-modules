variable "db_password" {
  type          = string
  description   = "The password for the database"
  default       = "1qazxsw2"
}

variable "db_name" {
  description   = "Name of the database"
  type          = string
  default       = "example"

}