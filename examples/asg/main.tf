terraform {
  # Require any 0.12.x version of Terraform
  required_version = ">= 0.12, < 0.13"
}


provider "aws" {
  region = "eu-west-2"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

module "asg" {
  source = "../../cluster/asg-rolling-deploy"

  cluster_name  = var.cluster_name
  ami           = "ami-04de2b60dd25fbb2e"
  instance_type = "t2.micro"
  min_size      = 1
  max_size      = 1
  subnet_ids    = data.aws_subnet_ids.default_vpc_subnets.ids
  custom_tags   = {
      example1_key  = true
      example2_key  = "example2_value"
      
  }
  server_port   = 8080
  
  
}

data "aws_vpc" "default_vpc" {
  default   = true
}

data "aws_subnet_ids" "default_vpc_subnets" {
  vpc_id    = data.aws_vpc.default_vpc.id
}

