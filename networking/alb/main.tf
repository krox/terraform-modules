terraform {
  # Require any 0.12.x version of Terraform
  required_version = ">= 0.12, < 0.13"
}


resource "aws_lb" "example" {
  name                = var.alb_name
  load_balancer_type  = "application"
  subnets             = var.subnet_ids
  security_groups     = [aws_security_group.example.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.example.arn
  port      = local.http_port
  protocol  = "HTTP"

  default_action { # anything that doesn't match the listener rule
    type = "fixed-response"
    fixed_response {
      content_type  = "text/plain"
      message_body  = "Fixed response content"
      status_code   = "200"
    }
  }
}


resource "aws_security_group" "example" {
  name        = "tf-${var.alb_name}_security_group"
  description = "Allow HTTP"
}

resource "aws_security_group_rule" "allow_http_inbound_alb" {
  type              = "ingress"
  security_group_id = aws_security_group.example.id
  from_port         = local.http_port #start range
  to_port           = local.http_port # end range
  protocol          = local.tcp_protocol
  cidr_blocks       = local.all_ips 
}

resource "aws_security_group_rule" "allow_all_outbound_alb" {
  type              = "egress"
  security_group_id = aws_security_group.example.id
  
  from_port         = local.any_port
  to_port           = local.any_port
  protocol          = local.any_protocol
  cidr_blocks       = local.all_ips
}


locals {
  http_port     = 80
  any_port      = 0
  any_protocol  = "-1"
  tcp_protocol  = "tcp"
  all_ips       = ["0.0.0.0/0"]
}