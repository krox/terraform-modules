terraform {
  # Require any 0.12.x version of Terraform
  required_version = ">= 0.12, < 0.13"
}


provider "aws" {
  region = "eu-west-2"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}


module "alb" {
  source = "../../networking/alb"
  
  alb_name      = var.alb_name
  subnet_ids    = data.aws_subnet_ids.default_vpc_subnets.ids
}

data "aws_vpc" "default_vpc" {
  default   = true
}

data "aws_subnet_ids" "default_vpc_subnets" {
  vpc_id    = data.aws_vpc.default_vpc.id
}

