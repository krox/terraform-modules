

variable "db_remote_state_bucket" {
  description = "Name of the s3 bucket for the database's rmote state"
  type        = string
}

variable "db_remote_state_key" {
  description = "Path for the database's remote state in S3"
  type        = string
}




variable "server_text" {
  description = "The text the web server should return"
  default     = "Hello, World"
  type        = string
}

variable "environment" {
  description = "The name of the environment we're deplyoing to"
  type        = string
}

variable "server_port" {
  description = "The port the server uses for HTTP requests"
  type        = number
  default     = 8080
}

variable "min_size" {
  description = "Min number of AMI instances in cluster"
  default = 2
}

variable "max_size" {
  description = "Max number of AMI instances in cluster"
  default = 4
}

variable "instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)"
  type        = string
}

variable "custom_tags" {
  description   = "Custom tags to set on the instance in the ASG"
  default       = {}
  type          = map(string)
}

variable "ami" {
  description = "The AMI to run in the cluster"
  default     = "ami-04de2b60dd25fbb2e"
  type        = string
}