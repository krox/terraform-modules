terraform {
  # Require any 0.12.x version of Terraform
  required_version = ">= 0.12, < 0.13"
}


data "template_file" "user_data" {
	template = file("${path.module}/user-data.sh")

	vars = {
		server_port = var.server_port
    db_address  = data.terraform_remote_state.database.outputs.db_address
    db_port     = data.terraform_remote_state.database.outputs.port
    server_text = var.server_text
	}
}

#get default vpc
data "aws_vpc" "default" {
  default = true # works as filter
}

#get default vpc subnets
data "aws_subnet_ids" "default_vpc" {
  vpc_id = data.aws_vpc.default.id # works as filter
}

data "terraform_remote_state" "database" {
  backend = "s3"
  config = {
    bucket  = var.db_remote_state_bucket
    key     = var.db_remote_state_key
    region  = "eu-west-2"
  }
}

resource "aws_lb_target_group" "web_servers" {
  name     = "hello-world-${var.environment}"
  port     = var.server_port
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 15
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener_rule" "http-any-forward-web_servers" {
  listener_arn = module.alb.alb_http_listener_arn
  #aws_lb_listener.http.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_servers.arn
  }

  condition {
    path_pattern {
      values  = ["*"]
    }
  }
}


module "asg" {
  source = "../../cluster/asg-rolling-deploy"

  cluster_name  = "hello-world-${var.environment}-asg"
  ami           = var.ami
  user_data     = data.template_file.user_data.rendered
  instance_type = var.instance_type

  min_size      = var.min_size
  max_size      = var.max_size

  subnet_ids        = data.aws_subnet_ids.default_vpc.ids
  target_group_arns = [aws_lb_target_group.web_servers.arn]
  health_check_type = "ELB"

  custom_tags   = var.custom_tags
  
}

module "alb" {
  source = "../../networking/alb"
  
  alb_name    = "hello-world-${var.environment}-alb"
  subnet_ids  = data.aws_subnet_ids.default_vpc.ids
}





